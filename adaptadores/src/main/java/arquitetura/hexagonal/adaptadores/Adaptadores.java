package arquitetura.hexagonal.adaptadores;

import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeAlteracaoDeCliente;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeBuscaDeCliente;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeConsultaDeClientes;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeExclusaoDeCliente;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeInsercaoDeCliente;
import arquitetura.hexagonal.aplicacao.portas.saida.RepositorioDeClientes;
import arquitetura.hexagonal.aplicacao.servicos.cliente.ServicoDeAlteracaoDeCliente;
import arquitetura.hexagonal.aplicacao.servicos.cliente.ServicoDeBuscaDeCliente;
import arquitetura.hexagonal.aplicacao.servicos.cliente.ServicoDeConsultaDeClientes;
import arquitetura.hexagonal.aplicacao.servicos.cliente.ServicoDeExclusaoDeCliente;
import arquitetura.hexagonal.aplicacao.servicos.cliente.ServicoDeInsercaoDeCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Adaptadores {
    @Autowired
    RepositorioDeClientes repositorioDeClientes;

    @Bean
    CasoDeUsoDeInsercaoDeCliente getCasoDeUsoDeInsercaoDeCliente() {
        return new ServicoDeInsercaoDeCliente(repositorioDeClientes);
    }

    @Bean
    CasoDeUsoDeAlteracaoDeCliente getCasoDeUsoDeAlteracaoDeCliente() {
        return new ServicoDeAlteracaoDeCliente(repositorioDeClientes);
    }

    @Bean
    CasoDeUsoDeExclusaoDeCliente getCasoDeUsoDeExclusaoDeCliente() {
        return new ServicoDeExclusaoDeCliente(repositorioDeClientes);
    }

    @Bean
    CasoDeUsoDeBuscaDeCliente getCasoDeUsoDeBuscaDeCliente() {
        return new ServicoDeBuscaDeCliente(repositorioDeClientes);
    }

    @Bean
    CasoDeUsoDeConsultaDeClientes getCasoDeUsoDeConsultaDeClientes() {
        return new ServicoDeConsultaDeClientes(repositorioDeClientes);
    }
}
