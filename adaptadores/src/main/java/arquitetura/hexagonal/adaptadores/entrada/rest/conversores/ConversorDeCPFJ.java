package arquitetura.hexagonal.adaptadores.entrada.rest.conversores;

import arquitetura.hexagonal.modelo.valores.CPFJ;
import org.springframework.http.HttpStatus;

import static arquitetura.hexagonal.adaptadores.entrada.rest.resposta.GeradorDeRespostaWeb.gerarErro;

public final class ConversorDeCPFJ {
    private ConversorDeCPFJ() {
    }

    public static CPFJ de(String cpfj) {
        if (cpfj == null) {
            throw gerarErro(HttpStatus.BAD_REQUEST, "CNPJ/CPF não informado.");
        }

        try {
            return CPFJ.de(cpfj);
        } catch (IllegalArgumentException failure) {
            throw gerarErro(HttpStatus.BAD_REQUEST, "CNPJ/CPF inválido.");
        }
    }
}
