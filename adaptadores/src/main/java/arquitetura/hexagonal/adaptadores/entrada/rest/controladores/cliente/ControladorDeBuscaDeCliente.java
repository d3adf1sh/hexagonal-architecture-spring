package arquitetura.hexagonal.adaptadores.entrada.rest.controladores.cliente;

import arquitetura.hexagonal.adaptadores.entrada.rest.conversores.ConversorDeCPFJ;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeBuscaDeCliente;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.ClienteNaoEncontrado;
import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static arquitetura.hexagonal.adaptadores.entrada.rest.resposta.GeradorDeRespostaWeb.gerarErro;

@RestController
@RequestMapping("/cliente")
public class ControladorDeBuscaDeCliente {
    private final CasoDeUsoDeBuscaDeCliente casoDeUsoDeBuscaDeCliente;

    public ControladorDeBuscaDeCliente(CasoDeUsoDeBuscaDeCliente casoDeUsoDeBuscaDeCliente) {
        this.casoDeUsoDeBuscaDeCliente = casoDeUsoDeBuscaDeCliente;
    }

    @GetMapping("/{cpfj}")
    public ClienteWeb buscar(@PathVariable("cpfj") String cpfj) {
        try {
            Cliente cliente = casoDeUsoDeBuscaDeCliente.buscar(ConversorDeCPFJ.de(cpfj));
            return ClienteWeb.converterParaWeb(cliente);
        } catch (ClienteNaoEncontrado failure) {
            throw gerarErro(HttpStatus.NOT_FOUND, failure.getMessage());
        }
    }
}
