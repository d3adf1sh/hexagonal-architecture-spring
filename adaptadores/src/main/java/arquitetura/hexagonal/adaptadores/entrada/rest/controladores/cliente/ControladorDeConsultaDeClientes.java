package arquitetura.hexagonal.adaptadores.entrada.rest.controladores.cliente;

import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeConsultaDeClientes;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static arquitetura.hexagonal.adaptadores.entrada.rest.resposta.GeradorDeRespostaWeb.gerarErro;

@RestController
@RequestMapping("/cliente")
public class ControladorDeConsultaDeClientes {
    private final CasoDeUsoDeConsultaDeClientes casoDeUsoDeConsultaDeClientes;

    public ControladorDeConsultaDeClientes(CasoDeUsoDeConsultaDeClientes casoDeUsoDeConsultaDeClientes) {
        this.casoDeUsoDeConsultaDeClientes = casoDeUsoDeConsultaDeClientes;
    }

    @GetMapping
    public List<ClienteWeb> consultar(@RequestParam(value = "nomeFantasia", required = false) String nomeFantasia) {
        try {
            return casoDeUsoDeConsultaDeClientes.consultar(nomeFantasia)
                    .stream()
                    .map(ClienteWeb::converterParaWeb)
                    .toList();
        } catch (IllegalArgumentException failure) { //TODO NullPointerException
            throw gerarErro(HttpStatus.BAD_REQUEST, failure.getMessage());
        }
    }
}