package arquitetura.hexagonal.adaptadores.entrada.rest.resposta;

public record RespostaWeb(int codigo, String mensagem) {
}
