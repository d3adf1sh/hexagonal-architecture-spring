package arquitetura.hexagonal.adaptadores.entrada.rest.resposta;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GerenciadorDeErroWeb {
    @ExceptionHandler(ErroWeb.class)
    public ResponseEntity<RespostaWeb> gerenciarErroWeb(ErroWeb erroWeb) {
        return erroWeb.getResposta();
    }
}
