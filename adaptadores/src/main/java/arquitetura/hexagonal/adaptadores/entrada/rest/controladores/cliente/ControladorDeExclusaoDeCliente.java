package arquitetura.hexagonal.adaptadores.entrada.rest.controladores.cliente;

import arquitetura.hexagonal.adaptadores.entrada.rest.conversores.ConversorDeCPFJ;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeExclusaoDeCliente;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.ClienteNaoEncontrado;
import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static arquitetura.hexagonal.adaptadores.entrada.rest.resposta.GeradorDeRespostaWeb.gerarErro;

@RestController
@RequestMapping("/cliente")
public class ControladorDeExclusaoDeCliente {
    private final CasoDeUsoDeExclusaoDeCliente casoDeUsoDeExclusaoDeCliente;

    public ControladorDeExclusaoDeCliente(CasoDeUsoDeExclusaoDeCliente casoDeUsoDeExclusaoDeCliente) {
        this.casoDeUsoDeExclusaoDeCliente = casoDeUsoDeExclusaoDeCliente;
    }

    @DeleteMapping("/{cpfj}")
    public ClienteWeb excluir(@PathVariable("cpfj") String cpfj) {
        try {
            Cliente cliente = casoDeUsoDeExclusaoDeCliente.excluir(ConversorDeCPFJ.de(cpfj));
            return ClienteWeb.converterParaWeb(cliente);
        } catch (ClienteNaoEncontrado failure) {
            throw gerarErro(HttpStatus.NOT_FOUND, failure.getMessage());
        }
    }
}
