package arquitetura.hexagonal.adaptadores.entrada.rest.controladores.cliente;

import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeAlteracaoDeCliente;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.ClienteNaoEncontrado;
import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static arquitetura.hexagonal.adaptadores.entrada.rest.resposta.GeradorDeRespostaWeb.gerarErro;

@RestController
@RequestMapping("/cliente")
public class ControladorDeAlteracaoDeCliente {
    private final CasoDeUsoDeAlteracaoDeCliente casoDeUsoDeAlteracaoDeCliente;

    public ControladorDeAlteracaoDeCliente(CasoDeUsoDeAlteracaoDeCliente casoDeUsoDeAlteracaoDeCliente) {
        this.casoDeUsoDeAlteracaoDeCliente = casoDeUsoDeAlteracaoDeCliente;
    }

    @PutMapping
    public ClienteWeb alterar(@RequestBody ClienteWeb clienteWeb) {
        try {
            Cliente cliente = casoDeUsoDeAlteracaoDeCliente.alterar(ClienteWeb.converterParaModelo(clienteWeb));
            return ClienteWeb.converterParaWeb(cliente);
        } catch (IllegalArgumentException failure) { //TODO NullPointerException
            throw gerarErro(HttpStatus.BAD_REQUEST, failure.getMessage());
        } catch (ClienteNaoEncontrado failure) {
            throw gerarErro(HttpStatus.NOT_FOUND, failure.getMessage());
        }
    }
}
