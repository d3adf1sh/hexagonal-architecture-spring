package arquitetura.hexagonal.adaptadores.entrada.rest.controladores.cliente;

import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeInsercaoDeCliente;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.ClienteJaCadastrado;
import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static arquitetura.hexagonal.adaptadores.entrada.rest.resposta.GeradorDeRespostaWeb.gerarErro;
import static arquitetura.hexagonal.adaptadores.entrada.rest.resposta.GeradorDeRespostaWeb.gerarResposta;

@RestController
@RequestMapping("/cliente")
public class ControladorDeInsercaoDeCliente {
    private final CasoDeUsoDeInsercaoDeCliente casoDeUsoDeInsercaoDeCliente;

    public ControladorDeInsercaoDeCliente(CasoDeUsoDeInsercaoDeCliente casoDeUsoDeInsercaoDeCliente) {
        this.casoDeUsoDeInsercaoDeCliente = casoDeUsoDeInsercaoDeCliente;
    }

    @PostMapping
    public ResponseEntity<ClienteWeb> inserir(@RequestBody ClienteWeb clienteWeb) {
        try {
            Cliente cliente = casoDeUsoDeInsercaoDeCliente.inserir(ClienteWeb.converterParaModelo(clienteWeb));
            return gerarResposta(HttpStatus.CREATED, ClienteWeb.converterParaWeb(cliente));
        } catch (IllegalArgumentException failure) { //TODO NullPointerException
            throw gerarErro(HttpStatus.BAD_REQUEST, failure.getMessage());
        } catch (ClienteJaCadastrado failure) {
            throw gerarErro(HttpStatus.CONFLICT, failure.getMessage());
        }
    }
}
