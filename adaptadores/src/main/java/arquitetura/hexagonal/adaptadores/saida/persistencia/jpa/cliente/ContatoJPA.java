package arquitetura.hexagonal.adaptadores.saida.persistencia.jpa.cliente;

import arquitetura.hexagonal.modelo.entidades.cliente.Contato;
import arquitetura.hexagonal.modelo.entidades.cliente.Telefone;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "Contato")
public class ContatoJPA {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idContato;
    @ManyToOne
    private ClienteJPA cliente;
    private String nome;
    @OneToMany(mappedBy = "contato", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TelefoneJPA> telefones;

    public static ContatoJPA converterParaJPA(ClienteJPA cliente, Contato contato) {
        ContatoJPA contatoJPA = new ContatoJPA();
        contatoJPA.setCliente(cliente);
        contatoJPA.setNome(contato.getNome());
        if (contato.getTelefones() != null) {
            contatoJPA.setTelefones(
                    contato.getTelefones().stream().map(telefone ->
                            TelefoneJPA.converterParaJPA(contatoJPA, telefone)).toList());
        }

        return contatoJPA;
    }

    public static Contato converterParaModelo(ContatoJPA contatoJPA) {
        List<Telefone> telefones = contatoJPA.getTelefones() != null
                ? contatoJPA.getTelefones().stream().map(TelefoneJPA::converterParaModelo).toList() : null;
        return new Contato(contatoJPA.getNome(), telefones);
    }
}