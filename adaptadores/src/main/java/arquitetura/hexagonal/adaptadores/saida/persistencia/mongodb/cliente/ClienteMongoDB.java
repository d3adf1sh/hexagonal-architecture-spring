package arquitetura.hexagonal.adaptadores.saida.persistencia.mongodb.cliente;

import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;
import arquitetura.hexagonal.modelo.entidades.cliente.Contato;
import arquitetura.hexagonal.modelo.valores.CPFJ;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Document("cliente")
public class ClienteMongoDB {
    @Id
    private String id;
    private String nomeFantasia;
    private String razaoSocial;
    private String cpfj;
    private List<ContatoMongoDB> contatos;
    private LocalDateTime dataDeInsercao;
    private LocalDateTime dataDeAlteracao;

    public static ClienteMongoDB converterParaMongoDB(Cliente cliente) {
        ClienteMongoDB clienteMongoDB = new ClienteMongoDB();
        clienteMongoDB.setNomeFantasia(cliente.getNomeFantasia());
        clienteMongoDB.setRazaoSocial(cliente.getRazaoSocial());
        clienteMongoDB.setCpfj(cliente.getCpfj() != null ? cliente.getCpfj().valor() : null);
        if (cliente.getContatos() != null) {
            clienteMongoDB.setContatos(
                    cliente.getContatos().stream().map(ContatoMongoDB::converterParaMongoDB).toList());
        }

        return clienteMongoDB;
    }

    public static Cliente converterParaModelo(ClienteMongoDB clienteMongoDB) {
        CPFJ cpfj = clienteMongoDB.getCpfj() != null ? new CPFJ(clienteMongoDB.getCpfj()) : null;
        List<Contato> contatos = clienteMongoDB.getContatos() != null
                ? clienteMongoDB.getContatos().stream().map(ContatoMongoDB::converterParaModelo).toList() : null;
        return new Cliente(clienteMongoDB.getNomeFantasia(), clienteMongoDB.getRazaoSocial(), cpfj, contatos);
    }
}