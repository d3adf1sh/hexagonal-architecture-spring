package arquitetura.hexagonal.adaptadores.saida.persistencia.jpa.cliente;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface RepositorioDeClientesJPA extends JpaRepository<ClienteJPA, Long> {
    @Query("SELECT c FROM ClienteJPA c WHERE c.cpfj = ?1")
    Optional<ClienteJPA> buscar(String cpfj);

    @Query("SELECT c FROM ClienteJPA c WHERE c.nomeFantasia like ?1")
    List<ClienteJPA> consultar(String nomeFantasia);
}