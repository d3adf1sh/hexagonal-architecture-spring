# To do

- Executar os bancos de dados em contêineres;
- Converter modelos através de um mapeador, se possível automatizado;
- Consolidar `modelo` e `aplicacao` em um único módulo com as packages `entidades`, `portas` e `servicos`;
- Remover o termo **Argumento** nas validações;
- Usar `IllegalArgumentException` ao invés de `NullPointerException` para argumentos nulos;
- Testar outras formas de validação (Notifications/Either, método público, factory, etc);
- Mover os adaptadores de entrada (`rest`) para a package `recursos` ou `pontosdeentrada`;
- Usar um repositório por operação;
- Ver anotação semelhante à do JPA para impedir o preenchimento de atributos ao inserir e alterar no MongoDB;
- Ajustar a **time-zone** para que os campos de data e hora não sejam persistidos com horas a mais no MongoDB;
- Não persistir o atributo `_class` no MongoDB;
- Indexar a busca por CPFJ no MongoDB;
- Remover os milissegundos ao persistir data e hora;
- Implementar adaptadores para Swing e serviço externo;
- Gerar DTOs de parâmetros através de um producer;
- Renomear o módulo `bootstrap` para `server` ou `configuration`;
- Usar constantes para dependências no Maven;
- TDD.
