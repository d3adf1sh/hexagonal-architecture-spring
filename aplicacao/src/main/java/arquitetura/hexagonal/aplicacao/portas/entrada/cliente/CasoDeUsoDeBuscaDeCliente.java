package arquitetura.hexagonal.aplicacao.portas.entrada.cliente;

import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;
import arquitetura.hexagonal.modelo.valores.CPFJ;

public interface CasoDeUsoDeBuscaDeCliente {
    Cliente buscar(CPFJ cpfj) throws ClienteNaoEncontrado;
}
