package arquitetura.hexagonal.aplicacao.portas.entrada.cliente;

import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;
import arquitetura.hexagonal.modelo.valores.CPFJ;

public interface CasoDeUsoDeExclusaoDeCliente {
    Cliente excluir(CPFJ cpfj) throws ClienteNaoEncontrado;
}
