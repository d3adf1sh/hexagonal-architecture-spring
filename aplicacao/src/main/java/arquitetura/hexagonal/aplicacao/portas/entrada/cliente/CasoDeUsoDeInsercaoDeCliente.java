package arquitetura.hexagonal.aplicacao.portas.entrada.cliente;

import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;

public interface CasoDeUsoDeInsercaoDeCliente {
    Cliente inserir(Cliente cliente) throws ClienteJaCadastrado;
}
