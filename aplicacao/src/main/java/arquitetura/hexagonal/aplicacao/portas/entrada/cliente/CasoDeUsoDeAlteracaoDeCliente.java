package arquitetura.hexagonal.aplicacao.portas.entrada.cliente;

import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;

public interface CasoDeUsoDeAlteracaoDeCliente {
    Cliente alterar(Cliente cliente) throws ClienteNaoEncontrado;
}
