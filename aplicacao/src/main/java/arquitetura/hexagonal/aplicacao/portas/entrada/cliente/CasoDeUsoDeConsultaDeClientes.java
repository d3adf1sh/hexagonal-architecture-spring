package arquitetura.hexagonal.aplicacao.portas.entrada.cliente;

import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;

import java.util.List;

public interface CasoDeUsoDeConsultaDeClientes {
    List<Cliente> consultar(String nomeFantasia);
}
