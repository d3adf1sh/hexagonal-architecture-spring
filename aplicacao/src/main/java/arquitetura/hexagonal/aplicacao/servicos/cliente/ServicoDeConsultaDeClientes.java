package arquitetura.hexagonal.aplicacao.servicos.cliente;

import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeConsultaDeClientes;
import arquitetura.hexagonal.aplicacao.portas.saida.RepositorioDeClientes;
import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;

import java.util.List;
import java.util.Objects;

public final class ServicoDeConsultaDeClientes implements CasoDeUsoDeConsultaDeClientes {
    private final RepositorioDeClientes repositorioDeClientes;

    public ServicoDeConsultaDeClientes(RepositorioDeClientes repositorioDeClientes) {
        this.repositorioDeClientes = repositorioDeClientes;
    }

    @Override
    public List<Cliente> consultar(String nomeFantasia) {
        Objects.requireNonNull(nomeFantasia, "O argumento \"nomeFantasia\" não pode ser nulo.");
        return repositorioDeClientes.consultar(nomeFantasia);
    }
}
