package arquitetura.hexagonal.aplicacao.servicos.cliente;

import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeBuscaDeCliente;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.ClienteNaoEncontrado;
import arquitetura.hexagonal.aplicacao.portas.saida.RepositorioDeClientes;
import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;
import arquitetura.hexagonal.modelo.valores.CPFJ;

import java.util.Objects;

public final class ServicoDeBuscaDeCliente implements CasoDeUsoDeBuscaDeCliente {
    private final RepositorioDeClientes repositorioDeClientes;

    public ServicoDeBuscaDeCliente(RepositorioDeClientes repositorioDeClientes) {
        this.repositorioDeClientes = repositorioDeClientes;
    }

    @Override
    public Cliente buscar(CPFJ cpfj) throws ClienteNaoEncontrado {
        Objects.requireNonNull(cpfj, "O argumento \"cpfj\" não pode ser nulo.");
        return repositorioDeClientes.buscar(cpfj)
                .orElseThrow(() -> new ClienteNaoEncontrado("Cliente %s não encontrado.".formatted(cpfj.formatar())));
    }
}
