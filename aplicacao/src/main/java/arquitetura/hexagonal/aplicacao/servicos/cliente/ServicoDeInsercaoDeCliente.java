package arquitetura.hexagonal.aplicacao.servicos.cliente;

import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.CasoDeUsoDeInsercaoDeCliente;
import arquitetura.hexagonal.aplicacao.portas.entrada.cliente.ClienteJaCadastrado;
import arquitetura.hexagonal.aplicacao.portas.saida.RepositorioDeClientes;
import arquitetura.hexagonal.modelo.entidades.cliente.Cliente;

import java.util.Objects;
import java.util.Optional;

public final class ServicoDeInsercaoDeCliente implements CasoDeUsoDeInsercaoDeCliente {
    private final RepositorioDeClientes repositorioDeClientes;

    public ServicoDeInsercaoDeCliente(RepositorioDeClientes repositorioDeClientes) {
        this.repositorioDeClientes = repositorioDeClientes;
    }

    @Override
    public Cliente inserir(Cliente cliente) throws ClienteJaCadastrado {
        Objects.requireNonNull(cliente, "O argumento \"cliente\" não pode ser nulo.");
        Optional<Cliente> clienteExistente = repositorioDeClientes.buscar(cliente.getCpfj());
        if (clienteExistente.isPresent()) {
            throw new ClienteJaCadastrado("Cliente %s já cadastrado.".formatted(cliente.getCpfj().formatar()));
        }

        return repositorioDeClientes.inserir(cliente);
    }
}
