package arquitetura.hexagonal.modelo.entidades.cliente;

import arquitetura.hexagonal.modelo.valores.CPFJ;
import lombok.Getter;

import java.util.List;
import java.util.Objects;

@Getter
public final class Cliente {
    private final String nomeFantasia;
    private final String razaoSocial;
    private final CPFJ cpfj;
    private final List<Contato> contatos;

    public Cliente(String nomeFantasia, String razaoSocial, CPFJ cpfj, List<Contato> contatos) {
        Objects.requireNonNull(nomeFantasia, "O argumento \"nomeFantasia\" não pode ser nulo.");
        if (nomeFantasia.isEmpty()) {
            throw new IllegalArgumentException("O argumento \"nomeFantasia\" não pode ser vazio.");
        }

        Objects.requireNonNull(razaoSocial, "O argumento \"razaoSocial\" não pode ser nulo.");
        if (razaoSocial.isEmpty()) {
            throw new IllegalArgumentException("O argumento \"razaoSocial\" não pode ser vazio.");
        }

        Objects.requireNonNull(cpfj, "O argumento \"cpfj\" não pode ser nulo.");
        Objects.requireNonNull(contatos, "O argumento \"contatos\" não pode ser nulo.");
        if (contatos.isEmpty()) {
            throw new IllegalArgumentException("O argumento \"contatos\" não pode ser vazio.");
        }

        this.nomeFantasia = nomeFantasia;
        this.razaoSocial = razaoSocial;
        this.cpfj = cpfj;
        this.contatos = contatos;
    }
}
